package stoliq.api;

import com.company.api.Client;
import com.company.api.Request;
import com.company.api.RequestRepo;
import com.company.api.Response;
import com.company.api.base.BaseTCAPI;
import org.testng.Assert;
import org.testng.annotations.Test;

public class APITestCase extends BaseTCAPI {

    @Test(dataProvider = "apiStatusCodeDataProvider")
    public void test_ResponseStatusCode(String path, int statusCode){
        Request request = RequestRepo.getPostman(path);
        Response response = new Client().send(request);
        Assert.assertEquals(response.getStatusCode().intValue(), statusCode, "invalid code");
    }

    @Test(dataProvider = "apiCurrencyDataProvider")
    public void test_CurrencyRate(String path, String currency){
        Request request = RequestRepo.getPostman(path);
        Response response = new Client().send(request);
        response.setBody(response.getBody().substring(1, response.getBody().length()-1));
        Assert.assertEquals(response.bodyToJSON().get("cc"), currency,"invalid response" );
    }

    @Test
    public void test_RandomDate(){
        Request request = RequestRepo.getPostman("NBUStatService/v1/statdirectory/banksincexp?date=20180101&period=1");
        Response response = new Client().send(request);
        response.setBody(response.getBody().substring(1, response.getBody().length()-1));
        Assert.assertEquals(response.bodyToJSON().get("dt"), "20090201");
    }
    @Test
    public void test_StatDirectory(){
        Request request = RequestRepo.getPostman("NBUStatService/v1/statdirectory/res?date=201708");
        Response response = new Client().send(request);
        response.setBody(response.getBody().substring(1, response.getBody().length()-1));
        Assert.assertEquals(response.bodyToJSON().get("dt"), "20170801");
    }

    @Test
    public void test_BalanceOfPayments(){
        Request request = RequestRepo.getPostman("NBUStatService/v1/statdirectory/balanceofpayments?start=20100201&end=20110101");
        Response response = new Client().send(request);
        response.setBody(response.getBody().substring(1, response.getBody().length()-1));
        Assert.assertEquals(response.bodyToJSON().get("value"), "375");
    }
}