package stoliq.ui;

import com.company.PropertyUtil;
import com.company.hibernate.HibernateUtil;
import com.company.hibernate.TestResult;
import com.company.stoliq.bo.LoginBO;
import com.company.stoliq.bo.ProxyBO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteProxyStoLiqTestCase {

    public void saveResults(Boolean result) {
        SessionFactory sessionFactory = null;
        try {
            //Get Session
            sessionFactory = new HibernateUtil().getSessionFactory();
            Session session = sessionFactory.getCurrentSession();
            //start transaction
            session.beginTransaction();
            TestResult testResult = new TestResult();
            testResult.setResult(result);
            System.out.println("address ID=" + session.save(testResult));
            //Commit transaction
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //terminate session factory, otherwise program won't end
            sessionFactory.close();
        }
    }

    @Test
    public void proxyDeletionTest() {
        LoginBO loginBO = new LoginBO();
        String login = (String) new PropertyUtil().getProperty("login");
        String pass = (String) new PropertyUtil().getProperty("password");

        loginBO.login(login, pass);

        ProxyBO proxyBO = new ProxyBO();
        String label = (String) new PropertyUtil().getProperty("label");

        boolean result;

        try {
            result = proxyBO.deleteProxy(label);
        } catch (NoSuchElementException e) {
            result = false;
        }
        this.saveResults(result);
        Assert.assertTrue(result);
    }

}
