package stoliq.ui;

import org.testng.annotations.DataProvider;

public class BaseTestCase {

    @DataProvider()
    public static Object[][] monitoringDataProvider(){
        return new Object[][]{{"5s"}, {"0.5s"}};
    }

    @DataProvider()
    public static Object[][] scrollKeysDataProvider(){
        return new Object[][]{{"Up"}, {"Down"}};
    }
}

