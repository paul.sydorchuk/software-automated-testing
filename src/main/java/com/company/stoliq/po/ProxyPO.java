package com.company.stoliq.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ProxyPO {
    WebDriver driver;

    public ProxyPO(WebDriver driver) {
        this.driver = driver;
    }

    public void addProxy() {
        WebElement addProxyButton = driver.findElement(By.xpath("//a[text()='Add Proxy']"));
        addProxyButton.click();
    }

    public void inputLabel(String label) {
        WebElement usernameInput = driver.findElement(By.xpath("//*[@name='label']"));

        WebElement waiterResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(usernameInput));

        waiterResult.sendKeys(label);
    }

    public void inputProxy(String proxy) {
        WebElement usernameInput = driver.findElement(By.xpath("//*[@name='proxy']"));

        WebElement waiterResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(usernameInput));

        waiterResult.sendKeys(proxy);
    }

    public void submit() {
        WebElement submitButton = driver.findElement(By.xpath("//*[@type='submit']"));
        submitButton.submit();
    }

    public void delete() {
        WebElement deleteButton = driver.findElement(By.xpath("//*[@class='fa fa-trash']"));
        deleteButton.click();
    }
}
