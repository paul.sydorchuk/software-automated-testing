package com.company.stoliq.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MonitoringPO {
    WebDriver driver;

    public MonitoringPO(WebDriver driver) {
        this.driver = driver;
    }

    public void changeInterval(String interval) {
        WebElement changeIntervalButton = driver.findElement(By.xpath(String.format("//a[text()='%s']", interval)));
        changeIntervalButton.click();
    }

}
