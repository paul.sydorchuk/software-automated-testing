package com.company.stoliq.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class InstancePO {
    WebDriver driver;

    public InstancePO(WebDriver driver) {
        this.driver = driver;
    }

    public void createInstance() {
        WebElement createInstanceButton = driver.findElement(By.xpath("//a[text()='Create Instance']"));
        createInstanceButton.click();
    }

    public void selectAlgorithm(String algorithm) {
        WebElement algorithmSelector = driver.findElement(By.xpath("//*[@name='algorithm']"));

        WebElement waiterResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(algorithmSelector));

        new Select(waiterResult).selectByVisibleText(algorithm);
    }

    public void selectProxy() {
        WebElement proxySelector = driver.findElement(By.xpath("//*[@name='proxy_id']"));

        WebElement waiterResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(proxySelector));

        new Select(waiterResult).selectByIndex(1);
    }

    public void inputCoefficient(String coefficient) {
        WebElement usernameInput = driver.findElement(By.xpath("//*[@name='coefficient']"));

        WebElement waiterResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(usernameInput));

        waiterResult.sendKeys(coefficient);
    }

    public void inputStep(String step) {
        WebElement usernameInput = driver.findElement(By.xpath("//*[@name='step']"));

        WebElement waiterResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(usernameInput));

        waiterResult.sendKeys(step);
    }

    public void inputLevel(String level) {
        WebElement usernameInput = driver.findElement(By.xpath("//*[@name='level']"));

        WebElement waiterResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(usernameInput));

        waiterResult.sendKeys(level);
    }

    public void inputPair(String pair) {
        WebElement usernameInput = driver.findElement(By.xpath("//*[@name='pair']"));

        WebElement waiterResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(usernameInput));

        waiterResult.sendKeys(pair);
    }

    public void submit() {
        WebElement submitButton = driver.findElement(By.xpath("//*[@type='submit']"));
        submitButton.submit();
    }
}
