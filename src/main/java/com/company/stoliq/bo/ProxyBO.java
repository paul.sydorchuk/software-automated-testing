package com.company.stoliq.bo;

import com.company.Driver;
import com.company.stoliq.po.ProxyPO;
import org.openqa.selenium.WebDriver;

public class ProxyBO {

    public boolean createProxy(String label, String proxy) {
        WebDriver driver = Driver.getInstance().getDriver();
        driver.get("http://stoliq.io/proxies");
        ProxyPO proxyPO = new ProxyPO(driver);
        proxyPO.addProxy();
        proxyPO.inputLabel(label);
        proxyPO.inputProxy(proxy);
        proxyPO.submit();
        return true;
    }

    public boolean deleteProxy(String label) {
        WebDriver driver = Driver.getInstance().getDriver();
        driver.get("http://stoliq.io/proxies");
        ProxyPO proxyPO = new ProxyPO(driver);
        proxyPO.delete();
        return true;
    }
}
