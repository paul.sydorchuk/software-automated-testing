package com.company.stoliq.bo;

import com.company.Driver;
import com.company.stoliq.po.InstancePO;
import com.company.stoliq.po.ProxyPO;
import org.openqa.selenium.WebDriver;

public class InstanceBO {

    public boolean createInstance(String algorithm, String coefficient, String step, String level, String pair) {
        WebDriver driver = Driver.getInstance().getDriver();
        driver.get("http://stoliq.io/instances");
        InstancePO instancePO = new InstancePO(driver);
        instancePO.createInstance();
        instancePO.selectAlgorithm(algorithm);
        instancePO.selectProxy();
        instancePO.inputCoefficient(coefficient);
        instancePO.inputStep(step);
        instancePO.inputLevel(level);
        instancePO.inputPair(pair);
        instancePO.submit();
        return true;
    }

//    public boolean deleteProxy(String label) {
//        WebDriver driver = Driver.getInstance().getDriver();
//        driver.get("http://stoliq.io/proxies");
//        ProxyPO proxyPO = new ProxyPO(driver);
//        proxyPO.delete();
//        return true;
//    }
}
