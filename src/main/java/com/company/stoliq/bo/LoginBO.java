package com.company.stoliq.bo;

import com.company.Driver;
import com.company.stoliq.po.LoginPO;
import org.openqa.selenium.WebDriver;

public class LoginBO {

    public boolean login(String login, String pass) {

        WebDriver driver = Driver.getInstance().getDriver();
        driver.get("http://stoliq.io/sign-in");
        LoginPO loginPO = new LoginPO(driver);
        loginPO.inputLogin(login);
        loginPO.inputPassword(pass);
        loginPO.submit();
        return true;
    }
}
