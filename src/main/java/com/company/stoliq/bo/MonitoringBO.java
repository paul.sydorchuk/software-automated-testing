package com.company.stoliq.bo;

import com.company.Driver;
import com.company.stoliq.po.MonitoringPO;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class MonitoringBO {

    public boolean checkMonitoring(String interval) {
        WebDriver driver = Driver.getInstance().getDriver();
        driver.get("http://stoliq.io/monitoring");
        MonitoringPO monitoringPO = new MonitoringPO(driver);
        try {
            monitoringPO.changeInterval(interval);
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }
}
