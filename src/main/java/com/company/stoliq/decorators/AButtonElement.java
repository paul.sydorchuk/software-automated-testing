package com.company.stoliq.decorators;

import org.openqa.selenium.WebElement;

public class AButtonElement extends CustomElement {

    public AButtonElement(WebElement webElement) {
        super(webElement);
    }

    public void click() {
        getWebElement().click();
    }
}
