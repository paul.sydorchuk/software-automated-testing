package com.company;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Driver {

    private static final Driver DRIVER = new Driver();

    public static Driver getInstance() {
        return DRIVER;
    }


    private WebDriver webDriver;

    public WebDriver getDriver() {
        String browser = (String) new PropertyUtil().getProperty("browser");
        if (webDriver == null) {
            switch (browser) {
                case "chrome" -> {
                    ChromeDriverManager.getInstance().setup();
                    webDriver = new ChromeDriver();
                }
                case "firefox" -> {
                    WebDriverManager.firefoxdriver().setup();
                    webDriver = new FirefoxDriver();
                }
                default -> throw new RuntimeException("invalid browser " + browser);
            }
        }
        return webDriver;
    }
}