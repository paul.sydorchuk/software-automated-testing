package com.company.api.base;

import org.testng.annotations.DataProvider;

public class BaseTCAPI {
    @DataProvider()
    public static Object[][] apiStatusCodeDataProvider() {
        return new Object[][]{{"NBUStatService/v1/statdirectory/exchange", 200},
                              {"bad", 404},
                              {"NBUStatService/v1/statdirectory/inflation?period=m&date=201501", 200}};
    }

    @DataProvider()
    public static Object[][] apiCurrencyDataProvider() {
        return new Object[][]{
                {"NBUStatService/v1/statdirectory/exchange?valcode=EUR&date=20200302", "EUR"},
                {"NBUStatService/v1/statdirectory/exchange?valcode=USD&date=20200302", "USD"}};
    }
}